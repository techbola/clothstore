<?php

namespace App\Http\Controllers;

use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class ShoppingController extends Controller
{

    public function addToCart()
    {
        $this->validate(\request(),[
            'qty' => 'required|min:1'
        ]);
        //dd(\request()->all());

        $product = Product::find(\request()->product_id);

        $featured = $product->featured;

        $cartItem = Cart::add([

            'id' => $product->id,
            'name' => $product->title,
            'qty' => \request()->qty,
            'price' => $product->price

        ]);

        Cart::associate($cartItem->rowId, 'App\Product');

        //dd($cartItem);

        //dd(Cart::content());

        return redirect()->route('cart');
    }

    public function cart()
    {
        //Cart::destroy();

        return view('front.cart');
    }

    public function cartDelete($id)
    {
        Cart::remove($id);

        return redirect()->back();
    }

    public function cartCheckout(Request $request)
    {

        $this->validate($request, [

            'fname' => 'required|string|min:3',
            'lname' => 'required|string|min:3',
            'email' => 'required|email',
            'phone' => 'required|numeric'

        ]);

        $data = array(
            'fname' => $request->fname,
            'lname' => $request->lname,
            'email' => $request->email,
            'phone' => $request->phone
        );

        Mail::send('emails.order', $data, function ($msg) use ($data)
        {

            $msg->from($data['email']);

            $msg->to('support@spacelifebrand.com');

            $msg->subject('New Order From The Website');

        });

        Session::flash('success', 'Order Sent, We would get back to you soon. Thanks');

        Cart::destroy();

//        foreach(Cart::content() as $item){
//
//            Cart::remove($item->id);
//
//        }

        return redirect()->route('index');

    }

}
