<!--footer start-->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 margin-b-30">
                <h3>About store</h3>
                <p>
                    Space Life Apparel is a Lifestyle Brand based out of Washington, D.C.
                    We are a collective group of high minded thinkers working towards a common goal.
                    Achieving a life outside of this world. Space Life, For minds higher than the skies.
                </p>
                <ul class="list-inline social-footer">
                    <li><a href="#" data-toggle="tooltip" data-placement="top" data-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" data-toggle="tooltip" data-placement="top" data-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="col-md-4 margin-b-30">
                <h3>Quick Links </h3>
                <div class="tags clearfix">
                    <a href="{{ route('index') }}">Home</a>
                    <a href="{{ route('store') }}">Shop</a>
                    <a href="{{ route('blog') }}">Blog</a>
                    <a href="{{ route('contact') }}">Contact</a>
                </div>
            </div>
            <div class="col-md-4 margin-b-30">
                <h3>Help Center</h3>
                <div class="media">
                    <div class="media-left">
                        <i class="fa fa-phone"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">(800) 202-7349</h4>
                    </div>
                </div>
                <div class="media">
                    <div class="media-left">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">
                            support@slbapparel.com
                        </h4>
                    </div>
                </div>
                <div class="media">
                    <div class="media-left">
                        <i class="fa fa-home"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">1420 N Street NW, Ste 102, Washington, DC 20005</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="footer-bottom">
    <div class="container text-center">
        <h3>
            <a href="{{ route('index') }}">
                <img src="{{ asset('main/images/Text.png') }}" alt="Space Life Apparel" class="logo_img">
            </a>
        </h3>
        <span class="copyright">&copy; Copyright 2018.</span>
    </div>
</div>
<!--footer end-->
